"""
Vowels Counter

This script defines a function, nbVoyelles, that counts the number of vowels in a given string. The function considers both standard ASCII characters and Unicode characters, converting them to their closest ASCII representation using the unidecode library.

Functions:
- nbVoyelles: Counts the number of vowels in a given string, considering both standard ASCII characters and Unicode characters.

Parameters:
- chaineAEvaluer (str): The input string to evaluate and count vowels in.

Returns:
- int: The count of vowels in the provided string.

Usage:
- Call the nbVoyelles function with a string to get the count of vowels.

Example:
    chaine = input("Enter a string to evaluate: ")
    vowel_count = nbVoyelles(chaine)
    print("Number of vowels: " + str(vowel_count))
"""

import unidecode

def nbVoyelles(chaineAEvaluer):
    voyellesLst = ["a", "e", "i", "o", "u"]
    compteur = 0
    newChaine = chaineAEvaluer.lower()
    newChaine = unidecode.unidecode(newChaine)

    for lettre in newChaine:
        if lettre in voyellesLst:
            compteur +=1
    return compteur



chaine = input("Entre chaine de caracyère à évaluer : ");

print("Nombre de voyelles : " + nbVoyelles(chaine));

